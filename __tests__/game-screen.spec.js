import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import App from '../App';


const goToGameScreen = (getByTestId, getAllByTestId, fireEvent) => {
  fireEvent.press(getByTestId("goToReadySetsButton"));
  fireEvent.press(getAllByTestId("levelItem")[0])
  fireEvent.press(getAllByTestId("categoryItem")[0])
  fireEvent.press(getAllByTestId("setItem")[0])
}

test('after clicking 5 times incorrect answer the first flashcard should be visible again', async () => {
  // Given
  const { getByTestId, getAllByTestId, findByTestId } = render(
    <App />
  );

  goToGameScreen(getByTestId, getAllByTestId, fireEvent)
  const expected = getByTestId("flashCard").props.children

  // When
  for(i = 0; i < 5; i++) {
  	 fireEvent.press(getByTestId("incorrectButton"));
  }

  // Then
  const actual = getByTestId("flashCard").props.children
  expect(actual).toEqual(expected)

});

test('after clicking 5 times correct answer user is navigated to summary screen ', async () => {
  // Given
  const { getByTestId, getAllByTestId, findByTestId } = render(
    <App />
  );

  goToGameScreen(getByTestId, getAllByTestId, fireEvent)

  // When
  for(i = 0; i < 5; i++) {
  	 fireEvent.press(getByTestId("correctButton"));
  }

  // Then
  const title = await waitFor(() => getByTestId("summaryScreenTitle"))
  expect(title).toBeDefined()

});
