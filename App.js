import * as React from "react";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AsyncStorage from "@react-native-community/async-storage";

import MenuScreen from "./src/screens/MenuScreen";
import LevelScreen from "./src/screens/LevelScreen";
import CategoryScreen from "./src/screens/CategoryScreen";
import SetsScreen from "./src/screens/SetsScreen";
import GameScreen from "./src/screens/GameScreen";
import GameSummaryScreen from "./src/screens/GameSummaryScreen";
import UserSetsScreen from "./src/screens/UserSetsScreen";
import CustomSetScreen from "./src/screens/CustomSetScreen";
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";
import common_pl from "./translations/pl/common.json";
import common_fr from "./translations/fr/common.json";
import common_en from "./translations/en/common.json";
import { useEffect, useReducer } from "react";
import appState from "./src/appState";
import appData from "./src/appData";
import { DATA_ACTIONS } from "./src/constants";
import StoreContext from "./src/StoreContext";
import GameReducer from "./src/reducers/GameReducer";
import { family } from "./src/data/pl-en/level-1/family";

const Stack = createStackNavigator();

i18next.init({
  interpolation: { escapeValue: false },
  lng: "pl",
  resources: {
    pl: {
      common: common_pl,
    },
    fr: {
      common: common_fr,
    },
    en: {
      common: common_en,
    },
  },
});

function NavigatorStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Menu" component={MenuScreen} />
      <Stack.Screen name="Level" component={LevelScreen} />
      <Stack.Screen name="Category" component={CategoryScreen} />
      <Stack.Screen name="Sets" component={SetsScreen} />
      <Stack.Screen name="Game" component={GameScreen} />
      <Stack.Screen
        name="GameSummary"
        component={GameSummaryScreen}
        options={{ headerLeft: null, left: null }}
      />
      <Stack.Screen name="UserSets" component={UserSetsScreen} />
      <Stack.Screen name="CustomSet" component={CustomSetScreen} />
    </Stack.Navigator>
  );
}

const getState = async () => {
  try {
    let state = await AsyncStorage.getItem("state");
    return state ? JSON.parse(state) : {};
  } catch (err) {
    console.log(err);
  }
};

const getData = async () => {
  try {
    let data = await AsyncStorage.getItem("data");
    return data ? JSON.parse(data) : {};
  } catch (err) {
    console.log(err);
  }
};

const getGameState = async () => {
  try {
    let gameState = await AsyncStorage.getItem("game");
    return gameState ? JSON.parse(gameState) : {};
  } catch (err) {
    console.log(err);
  }
};

const App = () => {
  const [state, dispatchState] = useReducer(appState, {
    unlockedLevels: 2,
    currentLevel: null,
    currentCategory: null,
    currentSet: null,
    currentCard: 1,
    learned: [],
    notLearned: [],
    points: 0,
  });
  const [data, dispatchData] = useReducer(appData, { flashCardSets: [] });
  const [game, dispatchGame] = useReducer(GameReducer, family);
  console.log("App STATE:", state);
  console.log("App DATA:", data);
  console.log("GAME STATE:", game);

  useEffect(() => {
    async function fetchState() {
      let receivedState = await getState();
      console.log("State received from device: ", receivedState);
      dispatchState({ type: "READ_STATE_FROM_DEVICE", payload: receivedState });
    }
    fetchState();
  }, []);

  useEffect(() => {
    async function fetchData() {
      let receivedData = await getData();
      console.log("Data received from device: ", receivedData);
      dispatchData({
        type: DATA_ACTIONS.read_data_from_device,
        payload: receivedData,
      });
    }
    fetchData();
  }, []);

  useEffect(() => {
    async function fetchGameState() {
      let receivedGameState = await getGameState();
      console.log("Game status received from device: ", receivedGameState);
      dispatchGame({
        type: "GET_GAME_STATE_FROM_DEVICE",
        payload: receivedGameState,
      });
    }
    fetchGameState();
  }, []);

  useEffect(() => {
    if (state) {
      console.log("Application state context:");
      console.log(JSON.stringify(state));
      AsyncStorage.setItem("state", JSON.stringify(state));
    }
  }, [state]);

  useEffect(() => {
    if (data) {
      console.log("Application data:");
      console.log(JSON.stringify(data));
      AsyncStorage.setItem("data", JSON.stringify(data));
    }
  }, [data]);

  useEffect(() => {
    if (game) {
      console.log("Application game:");
      console.log(JSON.stringify(game));
      AsyncStorage.setItem("game", JSON.stringify(game));
    }
  }, [game]);

  return (
    <StoreContext.Provider
      value={{ state, dispatchState, data, dispatchData, game, dispatchGame }}
    >
      <NavigationContainer>
        <I18nextProvider i18n={i18next}>
          <NavigatorStack />
        </I18nextProvider>
      </NavigationContainer>
    </StoreContext.Provider>
  );
}

export default App;