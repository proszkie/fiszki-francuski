import { STATUS } from "./constants";

export const LEVELS = [
  {
    id: 1,
    name: "level-A1",
    points: 0,
    total: 5,
    status: STATUS.unlocked,
  },
  {
    id: 2,
    name: "level-A2",
    points: 0,
    total: 10,
    status: STATUS.blocked,
  },
  {
    id: 3,
    name: "level-B1",
    total: 15,
    status: STATUS.blocked,
  },
  {
    id: 4,
    name: "level-B2",
    total: 20,
    status: STATUS.blocked,
  },
  {
    id: 5,
    name: "level-C1",
    total: 25,
    status: STATUS.blocked,
  },
  {
    id: 6,
    name: "level-C2",
    total: 25,
    status: STATUS.blocked,
  },
  {
    id: 7,
    name: "Insane xD",
    total: 25,
    status: STATUS.blocked,
  },
];
