export const family = {
  sets: [
    {
      set_id: 1,
      points: 0,
      learned: [],
      cards: [
        {
          card_id: 1,
          pl: "Mama",
          en: "Mom",
          learned: "False",
        },
        {
          card_id: 2,
          pl: "Tata",
          en: "Dad",
          learned: "False",
        },
        {
          card_id: 3,
          pl: "Siostra",
          en: "Sister",
          learned: "False",
        },
        {
          card_id: 4,
          pl: "Brat",
          en: "Brother",
          learned: "False",
        },
        {
          card_id: 5,
          pl: "Kuzyn",
          en: "Cousin",
          learned: "False",
        },
      ],
    },
    {
      set_id: 2,
      points: 0,
      learned: [],
      cards: [
        {
          card_id: 1,
          pl: "Rodzina",
          en: "Family",
          learned: "False",
        },
        {
          card_id: 2,
          pl: "Rodzice",
          en: "Parents",
          learned: "False",
        },
        {
          card_id: 3,
          pl: "Babcia",
          en: "Grandmother",
          learned: "False",
        },
        {
          card_id: 4,
          pl: "Dziadek",
          en: "Grandfather",
          learned: "False",
        },
        {
          card_id: 5,
          pl: "Dziadkowie",
          en: "Grandparents",
          learned: "False",
        },
        {
          card_id: 6,
          pl: "Wujek",
          en: "Uncle",
          learned: "False",
        },
        {
          card_id: 7,
          pl: "Dziecko",
          en: "Kid",
          learned: "False",
        },
        {
          card_id: 8,
          pl: "Urodziny",
          en: "Birthday",
          learned: "False",
        },
        {
          card_id: 9,
          pl: "Żona",
          en: "Wife",
          learned: "False",
        },
        {
          card_id: 10,
          pl: "Mąż",
          en: "Husband",
          learned: "False",
        },
      ],
    },
  ],
};
