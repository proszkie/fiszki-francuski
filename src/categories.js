import { STATUS } from "./constants";

export const categories = [
  {
    id: 1,
    name: "family",
    total: 5,
    status: STATUS.unlocked,
  },
  {
    id: 2,
    name: "food",
    total: 10,
    status: STATUS.unlocked,
  },
  {
    id: 3,
    name: "sports",
    total: 15,
    status: STATUS.unlocked,
  },
  {
    id: 4,
    name: "hobby",
    total: 20,
    status: STATUS.unlocked,
  },
  {
    id: 5,
    name: "holidays",
    total: 25,
    status: STATUS.unlocked,
  },
];
