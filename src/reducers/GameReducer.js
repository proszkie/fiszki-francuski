export default function GameReducer(state, action) {
  switch (action.type) {
    case "GET_GAME_STATE_FROM_DEVICE":
      return {
        ...state,
        ...action.payload,
      };
    case "FLASHCARD_LEARNED":
      return {
        ...state,
        ...(state.sets[action.userState.currentSet - 1].cards[
          action.id - 1
        ].learned = true),
        ...state.sets[action.userState.currentSet - 1].learned.push(action.id),
        ...(state.sets[action.userState.currentSet - 1].points += 1),
      };
    default:
      return state;
  }
}
