export const STATUS = {
  unlocked: "UNLOCKED",
  blocked: "BLOCKED",
  done: "DONE",
};

export const DATA_ACTIONS = {
  save_set: "SAVE_SET",
  remove_set: "REMOVE_SET",
  read_data_from_device: "READ_DATA_FROM_DEVICE",
};
