import { View, Button, StyleSheet, TouchableOpacity, Text } from "react-native";
import React from "react";

const CancelButton = ({ onPressAction, title }) => (
  <TouchableOpacity
    onPress={onPressAction}
    style={styles.cancelButtonContainer}
  >
    <Text style={styles.cancelButtonText}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cancelButtonContainer: {
    elevation: 8,
    backgroundColor: "#D3D3D3",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  cancelButtonText: {
    fontSize: 18,
    color: "#fff",
    alignSelf: "center",
  },
});

export default CancelButton;
