import React, {useState} from "react";
import {Icon, Tile} from "react-native-elements";
import {View, StyleSheet, Text, ImageBackground, TouchableOpacity} from "react-native";
import FlipCard from 'react-native-flip-card'
import {useTranslation} from "react-i18next";

const FlashCardItemTile = ({item,
                           setIsInEditingMode,
                           setWordOrPhrase,
                           setTranslation,
                           toggleOverlay,
                           isVisible,
                           setIsVisible,
                           flashCards,
                           setFlashCards,
                           setId}) => {

    const [isFlipped, setIsFlipped] = useState(false);
    const { t } = useTranslation("common");

    const edit = () => {
        setIsInEditingMode(true);
        toggleOverlay();
        setWordOrPhrase(item.wordOrPhrase);
        setTranslation(item.translation);
        setId(item.id);
        setIsFlipped(!isFlipped)
    }

    const remove = () => {
        const newFlashCards = [...flashCards].filter(f => f.id !== item.id);
        setFlashCards(newFlashCards);
    }

    return ( <View>
        <FlipCard flip={isFlipped}>
            <View style={styles.face}>
                <Tile
                    width={160}
                    imageSrc={require("../../assets/flashcard.jpg")}
                    featured
                    title={
                        item.wordOrPhrase && item.wordOrPhrase.length > 20
                            ? item.wordOrPhrase.substring(0, 20) + "..."
                            : item.wordOrPhrase
                    }
                    caption={
                        item.translation && item.translation.length > 20
                            ? item.translation.substring(0, 20) + "..."
                            : item.translation
                    }
                    onPress={() => {
                        setIsFlipped(!isFlipped)
                    }}
                    containerStyle={styles}
                />
            </View>
            <TouchableOpacity disabled={true} style={styles.back}>
                <ImageBackground style={styles.imageBackground} source={require('../../assets/flashcard.jpg')}>
                    <View style={styles.columnContainer}>
                        <TouchableOpacity onPress={edit} style={styles.edit}>
                            <Text style={styles.editRowItem} > {t('common.edit')}</Text>
                            <Icon color={'white'} style={styles.editRowItem} name='pencil' type='font-awesome'/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={remove} style={styles.delete}>
                            <Text style={styles.deleteRowItem}> {t('common.delete')}</Text>
                            <Icon color={'red'} style={styles.deleteRowItem} name='trash' type='font-awesome'/>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        </FlipCard>
    </View> )
};

const styles = StyleSheet.create({
    face: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    back: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    imageBackground: {
        width: 160,
        height: 128
    },
    columnContainer: {
        flexDirection: 'column',
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    edit: {
        flexDirection: 'row',
        textAlign: 'center',
        flex: 0.5
    },
    delete: {
        flexDirection: 'row',
        textAlign: 'center',
    },
    deleteRowItem: {
        flex: 0.5,
        fontSize: 20,
        color: 'red'
    },
    editRowItem: {
        flex: 0.5,
        fontSize: 20,
        color: 'white'
    },
});


export default FlashCardItemTile;