import React, { useContext, useEffect, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useTranslation } from "react-i18next";
import StoreContext from "../StoreContext";
import Spacer from "./Spacer";
import { useNavigation } from "@react-navigation/native";
import {Alert} from 'react-native';

/**
 *  Component responsible for displaying the card.
 * @param : {} currentSet the set on which the flashcard will operate
 */
const Flashcard = ({ currentSet }) => {
  const notLearnedFlashCards = currentSet.cards
    .filter((x) => {
      if (x.learned === "False") {
        return x.card_id;
      }
    })
    .map((x) => {
      return x.card_id;
    });

  const { t, i18n } = useTranslation("common");
  const { state, dispatchState } = useContext(StoreContext);
  const { game, dispatchGame } = useContext(StoreContext);
  const navigation = useNavigation();

  console.log("GAME: ", game);
  console.log("CURRENT SET INSIDE FLASHCARD", currentSet);
  console.log("USER STATE: ", state);
  const [counter, setCounter] = useState(0);
  const [language, setLanguage] = useState("pl");
  const [learned, setLearned] = useState(currentSet.learned);
  const [notLearned, setNotLearned] = useState(notLearnedFlashCards);
  const [displayCard, setDisplayCard] = useState(
    currentSet.learned.length === currentSet.cards.length
      ? "ALL_LEARNED"
      : currentSet.cards[notLearned[0] - 1]
  );

  console.log("LEARNED: ", learned);
  console.log("NOT LEARNED: ", notLearned);
  console.log("conuter: ", counter);
  console.log("displayCard: ", displayCard);

  useEffect(() => {
    if (notLearned.length === 0) {
      Alert.alert("Brawo! Udało Ci się nauczyć wszystkich słówek!");
      navigation.navigate("GameSummary");
    } else {
      if (counter >= notLearned.length) {
        setDisplayCard(currentSet.cards[notLearned[0] - 1]);
        setCounter(0);
      } else {
        setDisplayCard(currentSet.cards[notLearned[counter] - 1]);
      }
    }
  }, [counter, notLearned]);

  if (displayCard === "ALL_LEARNED") {
    return null;
  } else {
    return (
      <>
        <Text>Fiszka nr: {displayCard.card_id}</Text>
        <Text>learned: {displayCard.learned}</Text>
        <TouchableOpacity
          onPress={() => setLanguage(language === "pl" ? "en" : "pl")}
        >
          <View
              style={styles.container}>
            {language === "pl" ? (
              <Text
                  testID="flashCard"
                  style={styles.word}>{displayCard.pl}</Text>
            ) : (
              <Text
                  testID="flashCard"
                  style={styles.word}>{displayCard.en}</Text>
            )}
          </View>
        </TouchableOpacity>
        <Spacer />
        <View style={styles.buttons}>
          <TouchableOpacity
            testID="correctButton"
            onPress={() => {
              setLanguage("pl");
              dispatchState({ type: "CORRECT_ANSWER" });
              dispatchGame({
                type: "FLASHCARD_LEARNED",
                userState: state,
                id: displayCard.card_id,
              });
              setLearned([
                ...learned,
                currentSet.cards[notLearned[counter] - 1].card_id,
              ]);
              setNotLearned(
                notLearned.filter(
                  (x) => x !== currentSet.cards[notLearned[counter] - 1].card_id
                )
              );
              dispatchState({
                type: "SET_CURRENT_CARD",
                payload: currentSet.cards[counter].card_id,
              });
            }}
            style={styles.buttonCorrect}
          >
            <Text>Correct</Text>
          </TouchableOpacity>
          <TouchableOpacity
            testID="incorrectButton"
            onPress={() => {
              setLanguage("pl");
              setCounter(counter + 1);
              dispatchState({
                type: "SET_CURRENT_CARD",
                payload: currentSet.cards[counter].card_id,
              });
            }}
            style={styles.buttonIncorrect}
          >
            <Text>Incorrect</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#3c3c3c",
    marginHorizontal: 40,
    height: 260,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-around",
    height: 50,
  },
  buttonCorrect: {
    width: 120,
    backgroundColor: "green",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
  },
  buttonIncorrect: {
    width: 120,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
  },
  word: {
    color: "white",
    fontSize: 32,
  },
});

export default Flashcard;
