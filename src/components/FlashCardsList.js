import React from "react";
import { StyleSheet, FlatList, View } from "react-native";

class FlashCardsList extends React.Component {

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.flashCards !== this.props.flashCards;
  }

  render() {
    const flashCards = this.props.flashCards;
    const renderItem = this.props.renderItem;
    return (
      <View style={styles.flatListContainer}>
        <FlatList
          keyExtractor={(item, index) => item.id}
          contentContainerStyle={styles.flatList}
          data={flashCards}
          renderItem={renderItem}
          numColumns={2}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flatList: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "space-around",
  },
  flatListContainer: {
    height: "75%",
  },
});

export default FlashCardsList;
