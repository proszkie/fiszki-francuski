import {Tile} from "react-native-elements";
import React from "react";
import {StyleSheet} from "react-native";
import {useTranslation} from "react-i18next";

const AddNewItemTile = ({setIsInEditingMode, toggleOverlay}) => {

    const { t } = useTranslation("common");

    return (
        <Tile
            width={160}
            imageSrc={require("../../assets/flashcard.jpg")}
            containerStyle={styles.addItem}
            featured
            title={t("custom-set-screen.new-flashcard")}
            icon={{
                name: "plus-circle",
                type: "font-awesome",
                size: 45,
                iconStyle: styles.icon,
            }}
            onPress={() => {
                setIsInEditingMode(false);
                toggleOverlay();
            }}
            iconContainerStyle={styles.icon}
        />
    )
};

const styles = StyleSheet.create({
    addItem: {
        margin: 10,
    },
    icon: {
        color: "#009688",
    }
});

export default AddNewItemTile;