import React from "react";
import { Input, Overlay, Text } from "react-native-elements";
import { StyleSheet, View } from "react-native";
import SaveButton from "./SaveButton";
import CancelButton from "./CancelButton";
import { useTranslation } from "react-i18next";
import uuid from "react-native-uuid"; // TODO: find another id generator or fix security issues https://github.com/eugenehp/react-native-uuid/issues/11

const AddFLashCardOverlay = ({
  isInEditingMode,
  wordOrPhrase,
  setWordOrPhrase,
  translation,
  setTranslation,
  isVisible,
  toggleOverlay,
  setFlashCards,
  flashCardsWithoutLastElement,
}) => {
  const { t } = useTranslation("common");
  const overlayTitle = (isInEditingMode) => {
    const header = isInEditingMode
      ? t("custom-set-screen.overlay.edit-existing-flashcard")
      : t("custom-set-screen.overlay.add-new-flashcard");
    return (
      <Text h2 style={styles.heading}>
        {header}
      </Text>
    );
  };

  return (
    <Overlay isVisible={isVisible} onBackdropPress={toggleOverlay}>
      <View style={styles.overlay}>
        {overlayTitle(isInEditingMode)}
        <Input
          onChangeText={(text) => setWordOrPhrase(text)}
          placeholder={t("common.wordOrPhrase")}
          value={wordOrPhrase}
        />
        <Input
          onChangeText={(text) => setTranslation(text)}
          placeholder={t("common.translation")}
          value={translation}
        />

        <View style={styles.rowContainer}>
          <View style={styles.buttonContainer}>
            <CancelButton
              title={t("common.cancel")}
              onPressAction={toggleOverlay}
            />
          </View>
          <View style={styles.buttonContainer}>
            <SaveButton
              onPressAction={() => {
                const newFlashCard = {
                  id: uuid.v4(),
                  wordOrPhrase: wordOrPhrase,
                  translation: translation,
                };
                const newFlashCards = [
                  ...flashCardsWithoutLastElement(),
                  newFlashCard,
                  { addItem: true },
                ];
                setFlashCards(newFlashCards);
                setWordOrPhrase("");
                setTranslation("");
                toggleOverlay();
              }}
              title={t("common.save")}
            />
          </View>
        </View>
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    alignSelf: "flex-end",
    position: "absolute",
    bottom: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    flex: 1,
    margin: 5,
  },
  overlay: {
    height: 300,
    width: 300,
    alignItems: "center",
  },
  heading: {
    marginBottom: 35,
  },
});

export default AddFLashCardOverlay;
