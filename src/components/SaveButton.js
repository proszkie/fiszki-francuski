import { View, Button, StyleSheet, TouchableOpacity, Text } from "react-native";
import React from "react";

const SaveButton = ({ onPressAction, title }) => (
  <TouchableOpacity onPress={onPressAction} style={styles.saveButtonContainer}>
    <Text style={styles.saveButtonText}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  saveButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  saveButtonText: {
    fontSize: 18,
    color: "#fff",
    alignSelf: "center",
  },
});

export default SaveButton;
