import React, { useContext } from "react";
import {View, Text, StyleSheet, TouchableOpacity, FlatList} from "react-native";
import { useTranslation } from "react-i18next";
import { useNavigation } from "@react-navigation/native";
import { STATUS } from "../constants";
import StoreContext from "../StoreContext";

/**
 * Component used to render elements of list with buttons with background. Mainly for Level, Category, SetsScreen view.
 * @param title: string - title displayed on button
 * @returns {JSX.Element}
 */
const ButtonBackground = ({ title, navigateTo, action, testID }) => {
  const { t, i18n } = useTranslation("common");
  const navigation = useNavigation();
  const { dispatchState } = useContext(StoreContext);

  const Item = ({ title, testID }) =>
    title.status == STATUS.blocked ? (
      <View
          testID={testID}
          style={[styles.itemBase, styles.itemBlocked]}>
        <View>
          <Text style={styles.buttonText}>{title.name}</Text>
        </View>
        <Text style={styles.points}>3/15</Text>
      </View>
    ) : (
      <TouchableOpacity
        testID={testID}
        type="solid"
        onPress={() => {
          dispatchState({ type: action, payload: title.id });
          navigation.navigate(navigateTo);
        }}
      >
        <View
          style={[
            styles.itemBase,
            title.status == STATUS.done ? styles.itemDone : styles.itemUnlocked,
          ]}
        >
          <View>
            <Text style={styles.buttonText}>{title.name}</Text>
          </View>
          <Text style={styles.points}>X/15</Text>
        </View>
      </TouchableOpacity>
    );
  return <Item testID={testID} title={title} />;
};

const styles = StyleSheet.create({
  itemBase: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  itemDone: {
    backgroundColor: "#8cde7d",
  },
  itemUnlocked: {
    backgroundColor: "#ded57d",
  },
  itemBlocked: {
    backgroundColor: "#c3c3c3",
  },
  title: {
    fontSize: 32,
  },
  buttonText: {
    fontSize: 20,
    textAlign: "center",
  },
  points: {
    textAlign: "right",
    marginTop: -15,
    marginBottom: -5,
  },
});

export default ButtonBackground;
