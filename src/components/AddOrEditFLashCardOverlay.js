import React from "react";
import { Input, Overlay, Text } from "react-native-elements";
import { StyleSheet, View } from "react-native";
import SaveButton from "./SaveButton";
import CancelButton from "./CancelButton";
import { useTranslation } from "react-i18next";
import uuid from "react-native-uuid"; // TODO: find another id generator or fix security issues https://github.com/eugenehp/react-native-uuid/issues/11
import { useForm, Controller } from "react-hook-form";

const AddOrEditFLashCardOverlay = ({
  isInEditingMode,
  wordOrPhrase,
  translation,
  id,
  isVisible,
  toggleOverlay,
  setFlashCards,
  flashCardsWithoutLastElement,
}) => {
  const { t } = useTranslation("common");
  const overlayTitle = (isInEditingMode) => {
    const header = isInEditingMode
      ? t("custom-set-screen.overlay.edit-existing-flashcard")
      : t("custom-set-screen.overlay.add-new-flashcard");
    return (
      <Text h2 style={styles.heading}>
        {header}
      </Text>
    );
  };

    const { errors, handleSubmit, control } = useForm();

    const withEditedFlashCard = (newFlashCard) => {
        let withoutLastElement = [...flashCardsWithoutLastElement()]
        const indexOfEditedFlashCard = withoutLastElement.findIndex(f => f.id === id);
        withoutLastElement[indexOfEditedFlashCard] = newFlashCard;
        return [
            ...withoutLastElement,
            {addItem: true},
        ];
    }

    const withNewFlashCard = (newFlashCard) => {
        return [
            ...flashCardsWithoutLastElement(),
            newFlashCard,
            {addItem: true},
        ];
    }

    const onSubmit = (data) => {
        toggleOverlay();
        const newFlashCard = {
            id: isInEditingMode ? data.id : uuid.v4(),
            wordOrPhrase: data.wordOrPhrase,
            translation: data.translation,
        };
        let newFlashCards;
        if(isInEditingMode){
            newFlashCards = withEditedFlashCard(newFlashCard);
        } else {
            newFlashCards = withNewFlashCard(newFlashCard);
        }
        setFlashCards(newFlashCards);
    };


    return (
    <Overlay isVisible={isVisible} onBackdropPress={toggleOverlay}>
      <View style={styles.overlay}>
        {overlayTitle(isInEditingMode)}
          <Controller
              control={control}
              name="wordOrPhrase"
              rules={{ required: true }}
              defaultValue={wordOrPhrase ? wordOrPhrase : ""}
              render={(props) => (
                  <Input
                      {...props}
                      onChangeText={(wordOrPhrase) => props.onChange(wordOrPhrase)}
                      errorMessage={errors.wordOrPhrase && t('custom-set-screen.overlay.errors.word-or-phrase-required')}
                      placeholder={t("common.wordOrPhrase")}
                  />
              )}
          />

          <Controller
              control={control}
              name="translation"
              rules={{ required: true }}
              defaultValue={translation ? translation : ""}
              render={(props) => (
                  <Input
                      {...props}
                      onChangeText={(translation) => props.onChange(translation)}
                      placeholder={t("common.translation")}
                      errorMessage={errors.translation && t('custom-set-screen.overlay.errors.translation-required')}
                  />
              )}
          />


          <View style={styles.rowContainer}>
          <View style={styles.buttonContainer}>
            <CancelButton
              title={t("common.cancel")}
              onPressAction={toggleOverlay}
            />
          </View>
          <View style={styles.buttonContainer}>
            <SaveButton
              onPressAction={handleSubmit(onSubmit)}
              title={t("common.save")}
            />
          </View>
        </View>
      </View>
    </Overlay>
  );
};


const styles = StyleSheet.create({
  rowContainer: {
    alignSelf: "flex-end",
    position: "absolute",
    bottom: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    flex: 1,
    margin: 5,
  },
  overlay: {
    height: 300,
    width: 300,
    alignItems: "center",
  },
  heading: {
    marginBottom: 35,
  },
});


export default AddOrEditFLashCardOverlay;
