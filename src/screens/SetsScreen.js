import React from "react";
import { Text, FlatList } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useTranslation } from "react-i18next";
import ButtonBackground from "../components/ButtonBackground";
import { sets } from "../sets";

const renderItem = ({ item }) => {
  return (
    <ButtonBackground
      testID="setItem"
      title={item}
      navigateTo={"Game"}
      action={"SET_CURRENT_SET"}
    />
  );
};

const SetsScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");

  return (
    <SafeAreaView>
      <Text>{t("sets-screen.title")}</Text>
      <FlatList
        data={sets}
        keyExtractor={(item) => item.name}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
};

export default SetsScreen;
