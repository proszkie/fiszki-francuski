import React, { useContext } from "react";
import { Button, StyleSheet, ScrollView } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import Spacer from "../components/Spacer";
import { useTranslation } from "react-i18next";
import { ListItem, Text, Icon } from "react-native-elements";
import { DATA_ACTIONS } from "../constants";
import StoreContext from "../StoreContext";

const UserSetsScreen = ({ navigation }) => {
  const { t } = useTranslation("common");
  const { data, dispatchData } = useContext(StoreContext);

  const trashIcon = (id) => (
    <Icon
      name="trash"
      type="font-awesome"
      color="#e23e3e"
      onPress={() => dispatchData({ type: DATA_ACTIONS.remove_set, id: id })}
    />
  );

  const flashCardSets = data.flashCardSets;

  const Item = () => {
      return (
      flashCardSets.length > 0 ?
          <ScrollView>
              {flashCardSets.map((flashCardSet) => (
                  <ListItem
                      style={styles.listItem}
                      rightElement={trashIcon(flashCardSet.id)}
                      title={flashCardSet.title}
                      key={flashCardSet.id}
                      subtitle={
                          t("users-sets-screen.flashcards-amount") +
                          flashCardSet.flashCards.length
                      }
                      onPress={() => {
                          navigation.navigate("CustomSet", {flashCardSet: flashCardSet});
                      }}
                  />
              ))}
          </ScrollView>
          :
          <Text
            style={styles.emptyListMessage}>
              {t("users-sets-screen.emptyListMessage")}
          </Text>
      );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text h2>{t("users-sets-screen.title")}</Text>
      <Spacer />
      <Item/>
      <Button
        title={t("navigation.go-to-custom-set-creation")}
        style={styles.button}
        onPress={() => {
          navigation.navigate("CustomSet", { flashCardSet: {} });
        }}
      />
      <Spacer />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  listItem: {
    marginBottom: 10,
  },
  scrollView: {
    marginHorizontal: 20,
  },
  container: {
      flex: 1,
  },
  emptyListMessage: {
      alignSelf: "center",
      marginBottom: 20
  }
});

export default UserSetsScreen;
