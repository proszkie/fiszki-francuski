import React from "react";
import { useTranslation } from "react-i18next";
import { View, Text, StyleSheet, ImageBackground } from "react-native";
import Spacer from "../components/Spacer";
import { TouchableOpacity } from "react-native-gesture-handler";

const MenuScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");

  return (
    <View style={styles.container}>
      <Spacer />
      <TouchableOpacity
        type="solid"
        testID="goToReadySetsButton"
        title="Korzystaj z gotowych zestawów fiszek"
        onPress={() => {
          navigation.navigate("Level");
        }}
      >
        <ImageBackground
          blurRadius={4}
          source={require("../../assets/library.jpg")}
          style={styles.imgBackground}
        >
          <View style={styles.customButtonWithBackground}>
            <Text style={styles.buttonText}>
              {t("navigation.go-to-ready-made-flashcards")}
            </Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <Spacer />
      <TouchableOpacity
        type="solid"
        title="Korzystaj z gotowych zestawów fiszek"
        onPress={() => {
          navigation.navigate("UserSets");
        }}
      >
        <ImageBackground
          blurRadius={4}
          source={require("../../assets/flashcards.jpg")}
          style={styles.imgBackground}
        >
          <View style={styles.customButtonWithBackground}>
            <Text style={styles.buttonText}>
              {t("navigation.go-to-users-sets")}
            </Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  imgBackground: {
    width: "100%",
    height: 200,
    justifyContent: "center",
  },
  customButtonWithBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.40)",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: 20,
    color: "white",
    textAlign: "center",
  },
});

export default MenuScreen;
