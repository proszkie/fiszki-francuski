import React, { useContext } from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import Spacer from "../components/Spacer";
import { useTranslation } from "react-i18next";
import StoreContext from "../StoreContext";

const GameSummaryScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");
  const { state, dispatchState } = useContext(StoreContext);

  return (
    <SafeAreaView>
      <Text testID="summaryScreenTitle">
          {t("game-summary-screen.title")}
      </Text>
      <Spacer />
      <Button
        title={t("navigation.go-to-menu")}
        onPress={() => {
          navigation.navigate("Menu");
        }}
      />
      <Spacer />
      <Button
        color="red"
        title={"RESET STATE (btn for development tests)"}
        onPress={() => dispatchState({ type: "INIT_STATE" })}
      />
    </SafeAreaView>
  );
};

GameSummaryScreen.navigationOptions = {
  headerLeft: null,
};

export default GameSummaryScreen;
