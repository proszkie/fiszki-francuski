import React, { useContext} from "react";
import { Text, StyleSheet, FlatList } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import Spacer from "../components/Spacer";
import { useTranslation } from "react-i18next";
import ButtonBackground from "../components/ButtonBackground";
import { LEVELS } from "../levels";
import StoreContext from "../StoreContext";

const renderItem = ({ item }) => {
  return (
    <ButtonBackground
      testID="levelItem"
      title={item}
      navigateTo={"Category"}
      action={"SET_CURRENT_LEVEL"}
    />
  );
};

const LevelScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");
  const { game, dispatchGame } = useContext(StoreContext);

  return (
    <SafeAreaView style={styles.container}>
      <Text
          testID="levelScreenTitle"
      >
          {t("level-screen.title")}
      </Text>
      <FlatList
        data={LEVELS}
        keyExtractor={(item) => item.name}
        renderItem={renderItem}
      />
      <Spacer />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default LevelScreen;
