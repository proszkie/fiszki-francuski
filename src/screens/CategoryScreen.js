import React from "react";
import { Text, FlatList } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useTranslation } from "react-i18next";
import ButtonBackground from "../components/ButtonBackground";
import { categories } from "../categories";

const renderItem = ({ item }) => {
  return (
    <ButtonBackground
      testID="categoryItem"
      title={item}
      navigateTo={"Sets"}
      action={"SET_CURRENT_CATEGORY"}
    />
  );
};

const CategoryScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");

  return (
    <SafeAreaView>
      <Text>{t("category-screen.title")}</Text>
      <FlatList
        data={categories}
        keyExtractor={(item) => item.name}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
};

export default CategoryScreen;
