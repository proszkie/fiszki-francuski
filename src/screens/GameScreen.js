import React, { useContext } from "react";
import { Text, Button } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import Spacer from "../components/Spacer";
import { useTranslation } from "react-i18next";
import StoreContext from "../StoreContext";
import Flashcard from "../components/Flashcard";

const GameScreen = ({ navigation }) => {
  const { t, i18n } = useTranslation("common");
  const { state, dispatchState } = useContext(StoreContext);
  const { game, dispatchGame } = useContext(StoreContext);

  return (
    <SafeAreaView>
      <Text>{t("game-screen.title")}</Text>
      <Text>
        Wynik: {game.sets[state.currentSet - 1].points} /{" "}
        {game.sets[state.currentSet - 1].cards.length}
      </Text>
      <Spacer />
      <Flashcard currentSet={game.sets[state.currentSet - 1]} />
      <Spacer />
      {/*This button resets state to initial state when user first time run application*/}
      <Button
        color="red"
        title={"RESET STATE (btn for development tests)"}
        onPress={() => dispatchState({ type: "INIT_STATE" })}
      />
      <Spacer />
      <Button
        title={t("navigation.go-to-game-summary")}
        onPress={() => {
          navigation.navigate("GameSummary");
        }}
      />
    </SafeAreaView>
  );
};

export default GameScreen;
