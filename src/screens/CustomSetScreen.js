import React, { useState, useContext } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { useTranslation } from "react-i18next";
import { Input, Tile } from "react-native-elements";
import { StyleSheet, View } from "react-native";
import SaveButton from "../components/SaveButton";
import CancelButton from "../components/CancelButton";
import FlashCardsList from "../components/FlashCardsList";
import uuid from "react-native-uuid"; // TODO: find another id generator or fix security issues https://github.com/eugenehp/react-native-uuid/issues/11
import { DATA_ACTIONS } from "../constants";
import StoreContext from "../StoreContext";
import AddOrEditFLashCardOverlay from "../components/AddOrEditFLashCardOverlay";
import FlashCardItemTile from "../components/FlashCardItemTile";
import AddNewItemTile from "../components/AddNewItemTile";
import { useForm, Controller } from "react-hook-form";

const CustomSetScreen = ({ route, navigation }) => {
  const { flashCardSet } = route.params;
  const addItem = { addItem: true };
  const { t } = useTranslation("common");
  const title = flashCardSet.title ? flashCardSet.title : ""
  const [flashCards, setFlashCards] = flashCardSet.flashCards
    ? useState([...flashCardSet.flashCards, addItem])
    : useState([addItem]);
  const [isVisible, setIsVisible] = useState(false);
  const [isInEditingMode, setIsInEditingMode] = useState(false);
  const { dispatchData } = useContext(StoreContext);
  const [wordOrPhrase, setWordOrPhrase] = useState('');
  const [translation, setTranslation] = useState('');
  const [id, setId] = useState('');

  const toggleOverlay = () => {
    setWordOrPhrase("");
    setTranslation("");
    setId("");
    setIsVisible(!isVisible);
  };

  const renderItem = ({item}) =>
      item.addItem ?
          <AddNewItemTile
              setIsInEditingMode={setIsInEditingMode}
              toggleOverlay={toggleOverlay}/>
          : <FlashCardItemTile
              item={item}
              setIsInEditingMode={setIsInEditingMode}
              setWordOrPhrase={setWordOrPhrase}
              setTranslation={setTranslation}
              isVisible={isVisible}
              setIsVisible={setIsVisible}
              toggleOverlay={toggleOverlay}
              flashCards={flashCards}
              setFlashCards={setFlashCards}
              setId={setId}
          />

  const flashCardsWithoutLastElement = () => {
    const newFlashCards = [...flashCards];
    newFlashCards.pop();
    return newFlashCards;
  };


  const { control, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    dispatchData({
      type: DATA_ACTIONS.save_set,
      id: flashCardSet.id ? flashCardSet.id : uuid.v4(),
      title: data.title,
      flashCards: flashCardsWithoutLastElement(),
    });
    navigation.navigate("UserSets");
  };

  return (
    <SafeAreaView style={styles.columnContainer}>
      <AddOrEditFLashCardOverlay
        isInEditingMode={isInEditingMode}
        wordOrPhrase={wordOrPhrase}
        translation={translation}
        id={id}
        isVisible={isVisible}
        toggleOverlay={toggleOverlay}
        setFlashCards={setFlashCards}
        flashCardsWithoutLastElement={flashCardsWithoutLastElement}
      />

      <Controller
        control={control}
        name="title"
        defaultValue={title}
        rules={{required: true}}
        render={(props) => (
          <Input
            {...props}
            errorMessage={errors.title && t('custom-set-screen.errors.title-required')}
            onChangeText={(value) => props.onChange(value)}
            placeholder={t("custom-set-screen.new-set-title")}
          />
        )}
      />

      <FlashCardsList renderItem={renderItem} flashCards={flashCards} />

      <View style={styles.rowContainer}>
        <View style={styles.buttonContainer}>
          <CancelButton
            title={t("common.cancel")}
            onPressAction={() => navigation.navigate("UserSets")}
          />
        </View>
        <View style={styles.buttonContainer}>
          <SaveButton
            title={t("common.save")}
            onPressAction={handleSubmit(onSubmit)}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  columnContainer: {
    flex: 1,
    flexDirection: "column",
  },
  rowContainer: {
    alignSelf: "flex-end",
    position: "absolute",
    bottom: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    flex: 1,
    margin: 5,
  },
  item: {
    margin: 10,
  },
  addItem: {
    margin: 10,
  },
  icon: {
    color: "#009688",
  },
});

export default CustomSetScreen;
