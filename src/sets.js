import { STATUS } from "./constants";

export const sets = [
  {
    id: 1,
    name: "1",
    total: 5,
    status: STATUS.done,
  },
  {
    id: 2,
    name: "2",
    total: 10,
    status: STATUS.unlocked,
  },
  {
    id: 3,
    name: "3",
    total: 15,
    status: STATUS.unlocked,
  },
  {
    id: 4,
    name: "4",
    total: 20,
    status: STATUS.unlocked,
  },
  {
    id: 5,
    name: "5",
    total: 25,
    status: STATUS.unlocked,
  },
];
