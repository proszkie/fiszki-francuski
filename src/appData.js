import { DATA_ACTIONS } from "./constants";

export default function reducer(data, action) {
  switch (action.type) {
    case DATA_ACTIONS.read_data_from_device:
      return {
        ...data,
        ...action.payload,
      };
    case DATA_ACTIONS.save_set:
      return Object.assign({}, data, {
        flashCardSets: [
          ...data.flashCardSets.filter(
            (flashCardSet) => flashCardSet.id !== action.id
          ),
          {
            id: action.id,
            title: action.title,
            flashCards: action.flashCards,
          },
        ],
      });
    case DATA_ACTIONS.remove_set:
      return Object.assign({}, data, {
        flashCardSets: [
          ...data.flashCardSets.filter(
            (flashCardSet) => flashCardSet.id !== action.id
          ),
        ],
      });
    default:
      return { ...data };
  }
}
