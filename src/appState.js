export default function appState(state, action) {
  switch (action.type) {
    case "READ_STATE_FROM_DEVICE":
      return {
        ...state,
        ...action.payload,
      };
    case "INIT_STATE":
      return {
        unlockedLevels: 2,
        currentLevel: 1,
        currentCategory: 1,
        currentSet: "Family",
        currentCard: 1,
        points: 0,
      };
    case "SET_CURRENT_LEVEL":
      return {
        ...state,
        currentLevel: action.payload,
      };
    case "SET_CURRENT_CATEGORY":
      return {
        ...state,
        currentCategory: action.payload,
      };
    case "SET_CURRENT_SET":
      return {
        ...state,
        currentSet: action.payload,
      };
    case "LEVEL_UP":
      return {
        ...state,
        currentLevel: state.currentLevel + 1,
      };
    case "CORRECT_ANSWER":
      return {
        ...state,
        points: state.points + 1,
      };
    case "INCORRECT_ANSWER": //todo
      return {
        ...state,
        ...action.newState,
      };
    case "UPDATE_STATE":
      return {
        ...action.payload,
      };
    default:
      return { ...state };
  }
}
